/* Folder Applet - folder-menu.h
 *
 * Copyright (C) 2004 Christian Neumair <chris@gnome-de.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef FOLDER_MENU_H
#define FOLDER_MENU_H

#include <gtk/gtkmenu.h>

#define TYPE_FOLDER_MENU            (folder_menu_get_type ())
#define FOLDER_MENU(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_FOLDER_MENU, FolderMenu))
#define FOLDER_MENU_CLASS(class)    (G_TYPE_CHECK_CLASS_CAST ((class),  TYPE_FOLDER_MENU, FolderMenuClass))
#define IS_FOLDER_MENU(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_FOLDER_MENU))
#define IS_FOLDER_MENU_CLASS(class) (G_TYPE_CHECK_CLASS_TYPE ((class),  TYPE_FOLDER_MENU))
#define FOLDER_MENU_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  TYPE_FOLDER_MENU, FolderMenuClass))

typedef struct _FolderMenu      FolderMenu;
typedef struct _FolderMenuClass FolderMenuClass;

struct _FolderMenu {
	GtkMenu parent;
};

struct _FolderMenuClass {
	GtkMenuClass parent_class;
};

GtkWidget *folder_menu_new (void);

#endif

/* Folder Applet - folder-menu.c
 *
 * Copyright (C) 2004 Christian Neumair <chris@gnome-de.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "folder-menu-item.h"
#include "folder-menu.h"

#include <gdk/gdkkeysyms.h>
#include <gtk/gtkmenu.h>

GtkMenuClass *parent_class = NULL;

G_DEFINE_TYPE (FolderMenu, folder_menu, GTK_TYPE_MENU);

static gboolean
folder_menu_real_activate (GtkMenu *menu)
{
	GtkWidget *parent;

	parent = menu->parent_menu_item;
	if (!parent ||
	    !IS_FOLDER_MENU_ITEM (parent) ||
	    GTK_MENU_SHELL (menu)->active_menu_item)
		return FALSE;

	folder_menu_item_set_activation_permitted (FOLDER_MENU_ITEM (parent), TRUE);

	gtk_menu_item_activate (GTK_MENU_ITEM (parent));

	gtk_menu_shell_deactivate (GTK_MENU_SHELL (menu));

	return TRUE;
}

static gboolean
folder_menu_button_press_event (GtkWidget      *widget,
				GdkEventButton *event)
{
	GdkWindow *ptr_window;

	/* don't handle clicks that occured outside any menu widget */
	ptr_window = gdk_window_at_pointer (NULL, NULL);
	if (!ptr_window)
		return FALSE;

	return folder_menu_real_activate (GTK_MENU (widget));
}

static gboolean
folder_menu_key_press_event (GtkWidget      *widget,
			     GdkEventKey    *event)
{
	if ((event->keyval == GDK_Return || event->keyval == GDK_ISO_Enter) &&
	     folder_menu_real_activate (GTK_MENU (widget)))
		return TRUE;

	return GTK_WIDGET_CLASS (parent_class)->key_press_event (widget, event);
}

static void
folder_menu_init (FolderMenu *menu)
{
}

static void
folder_menu_class_init (FolderMenuClass *class)
{
	GtkWidgetClass *widget_class;

	widget_class = GTK_WIDGET_CLASS (class);
	widget_class->button_press_event = folder_menu_button_press_event;
	widget_class->key_press_event = folder_menu_key_press_event;

	parent_class = g_type_class_peek_parent (class);
}

GtkWidget *
folder_menu_new (void)
{
	return g_object_new (TYPE_FOLDER_MENU, NULL);
}

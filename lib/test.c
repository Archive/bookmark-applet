#include "folder-menu.h"
#include "folder-menu-item.h"

#include <gtk/gtkmain.h>
#include <gtk/gtkmenu.h>
#include <gtk/gtkmenuitem.h>
#include <gtk/gtkmenushell.h>
#include <gtk/gtkwidget.h>

#include <libgnomevfs/gnome-vfs-init.h>

int
main (int    argc,
      char **argv)
{
	GtkWidget *menu;
	GtkWidget *menuitem;

	gtk_init (&argc, &argv);
	gnome_vfs_init ();

	menu = gtk_menu_new ();

	menuitem = folder_menu_item_new ("file:///",
					 "Browse Root",
					 NULL);
	gtk_widget_show (menuitem);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);

	menuitem = folder_menu_item_new ("file:///home",
					 "Browse Home Folders",
					 NULL);
	gtk_widget_show (menuitem);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);

	gtk_menu_popup (GTK_MENU (menu),
			NULL, NULL, NULL, NULL, 0, GDK_CURRENT_TIME);

	gtk_main ();
}

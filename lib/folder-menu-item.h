/* Folder Applet - folder-menu-item.h
 *
 * Copyright (C) 2004 Christian Neumair <chris@gnome-de.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef FOLDER_MENU_ITEM_H
#define FOLDER_MENU_ITEM_H

#include <gtk/gtkimagemenuitem.h>

#define TYPE_FOLDER_MENU_ITEM            (folder_menu_item_get_type ())
#define FOLDER_MENU_ITEM(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_FOLDER_MENU_ITEM, FolderMenuItem))
#define FOLDER_MENU_ITEM_CLASS(class)    (G_TYPE_CHECK_CLASS_CAST ((class),  TYPE_FOLDER_MENU_ITEM, FolderMenuItemClass))
#define IS_FOLDER_MENU_ITEM(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_FOLDER_MENU_ITEM))
#define IS_FOLDER_MENU_ITEM_CLASS(class) (G_TYPE_CHECK_CLASS_TYPE ((class),  TYPE_FOLDER_MENU_ITEM))
#define FOLDER_MENU_ITEM_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  TYPE_FOLDER_MENU_ITEM, FolderMenuItemClass))

typedef struct _FolderMenuItem      FolderMenuItem;
typedef struct _FolderMenuItemClass FolderMenuItemClass;

struct _FolderMenuItem {
	GtkImageMenuItem parent;

	gchar *uri;
};

struct _FolderMenuItemClass {
	GtkImageMenuItemClass parent_class;
};

GtkWidget *folder_menu_item_new (const gchar *uri,
				 const gchar *name,
				 const gchar *icon_name);

void       folder_menu_item_set_activation_permitted (FolderMenuItem *menuitem,
						      gboolean        activation_permitted);

#endif

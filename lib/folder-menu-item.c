/* Folder Applet - folder-menu-item.c
 *
 * Copyright (C) 2004 Christian Neumair <chris@gnome-de.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
 #include <config.h> /* Internationalization */
#endif

#include "folder-menu-item.h"
#include "folder-menu.h"

#include <unistd.h> /* for access() */
#include <glib/gbacktrace.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <libgnomeui/gnome-icon-lookup.h>
#include <libgnomevfs/gnome-vfs-directory.h>
#include <libgnomevfs/gnome-vfs-mime-utils.h>
#include <libgnomevfs/gnome-vfs-monitor.h>
#include <libgnomevfs/gnome-vfs-utils.h>

/* Static Methods */
static void folder_menu_item_activate   (GtkMenuItem           *menuitem);
static void folder_menu_item_init       (FolderMenuItem      *menuitem);
static void folder_menu_item_class_init (FolderMenuItemClass *class);
static void folder_menu_item_finalize   (GObject               *object);

G_DEFINE_TYPE (FolderMenuItem, folder_menu_item, GTK_TYPE_IMAGE_MENU_ITEM);

static GtkImageMenuItemClass *parent_class = NULL;

#define FOLDER_MENU_ITEM_GET_PRIVATE(obj) \
	(G_TYPE_INSTANCE_GET_PRIVATE ((obj), TYPE_FOLDER_MENU_ITEM, FolderMenuItemPrivate))

typedef struct _FolderMenuItemPrivate FolderMenuItemPrivate;

struct _FolderMenuItemPrivate {
	gboolean activation_permitted; /* eeek! GtkMenuShell is broken!
					  It activates after popping up the submenu */
	gboolean update_submenu;

	GnomeVFSMonitorHandle *monitor_handle;
};

void
folder_menu_item_set_activation_permitted (FolderMenuItem *menuitem,
					     gboolean activation_permitted)
{
	FolderMenuItemPrivate *priv;

	priv = FOLDER_MENU_ITEM_GET_PRIVATE (menuitem);
	priv->activation_permitted = activation_permitted;
}

static void
folder_menu_item_activate (GtkMenuItem *menuitem)
{
	FolderMenuItemPrivate *priv;

	GnomeVFSResult   res;
	GtkWidget       *error_dialog;
	gchar           *tmp, *tmp2;

	priv = FOLDER_MENU_ITEM_GET_PRIVATE (menuitem);

	if (!priv->activation_permitted)
		return;

	res = gnome_vfs_url_show (FOLDER_MENU_ITEM (menuitem)->uri);

	if (res == GNOME_VFS_OK)
		return;

	tmp = g_strdup_printf (_("Could Not Open %s"),
			       FOLDER_MENU_ITEM (menuitem)->uri);
	tmp2 = g_strdup_printf (_("Details: %s"),
				gnome_vfs_result_to_string (res));

	error_dialog = gtk_message_dialog_new_with_markup (NULL,
							   0,
							   GTK_MESSAGE_ERROR,
							   GTK_BUTTONS_CLOSE,
							   "<big><b>%s</b></big>\n\n%s",
							   tmp, tmp2);
	gtk_window_set_title (GTK_WINDOW (error_dialog), "");

	g_signal_connect (error_dialog, "response",
			  G_CALLBACK (gtk_widget_destroy),
			  NULL);
	g_signal_connect (error_dialog, "destroy",
			  G_CALLBACK (gtk_widget_destroy),
			  NULL);
	gtk_dialog_run (GTK_DIALOG (error_dialog));

	g_free (tmp);
	g_free (tmp2);
}

static gint
insert_sorted (const gchar *a,
	       const gchar *b)
{
	gchar *colla, *collb;
	gint result;

	g_assert (a);
	g_assert (b);

	colla = g_utf8_collate_key (a, -1);
	collb = g_utf8_collate_key (b, -1);

	result = strcmp (colla, collb);

	g_free (colla);
	g_free (collb);

	return result;
}

static void
update_submenu (FolderMenuItem *menuitem)
{
	FolderMenuItemPrivate *priv;

	GnomeVFSDirectoryHandle *handle;
	GnomeVFSFileInfo *info;
	GnomeVFSResult result;

	GtkWidget *submenu, *submenuitem;

	gchar *abspath;
	GList *uris = NULL, *l;

	gtk_menu_item_remove_submenu (GTK_MENU_ITEM (menuitem));

	gnome_vfs_directory_open (&handle, menuitem->uri,
				  GNOME_VFS_FILE_INFO_GET_MIME_TYPE);

	info = gnome_vfs_file_info_new ();

	while (1) {
		gnome_vfs_file_info_clear (info);
		result = gnome_vfs_directory_read_next (handle, info);

		if (result != GNOME_VFS_OK) {
			break;
		}

		g_assert (info);

		if ((info->valid_fields & GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE) != 0 &&
		    info->name[0] != '.' &&
		    !strcmp (gnome_vfs_file_info_get_mime_type (info), "x-directory/normal"))
			uris = g_list_insert_sorted (uris, g_strdup (info->name),
						     (GCompareFunc) insert_sorted);

	}

	gnome_vfs_file_info_unref (info);
	gnome_vfs_directory_close (handle);

	if (!uris)
		return;

	submenu = folder_menu_new ();
	gtk_widget_show (submenu);

	for (l = uris; l; l = l->next) {
		abspath = g_build_path (G_DIR_SEPARATOR_S, menuitem->uri, l->data, NULL);

		submenuitem = folder_menu_item_new (abspath,
						    l->data,
						    NULL);
		gtk_menu_shell_append (GTK_MENU_SHELL (submenu), submenuitem);
		gtk_widget_show (submenuitem);

		g_free (l->data);
	}

	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem), submenu);

	g_list_free (uris);
}

static gboolean
string_contains_uri_delimiter (gchar *string)
{
	gchar *s;

	g_assert (string);

	for (s = string; s; s++) {
		switch (*s) {
			case ':':
				return TRUE;

			case '\0':
				return FALSE;

			default:
				break;
		}
	}

	g_assert_not_reached ();
}

static gboolean
read_ok (const gchar *uri)
{
	gchar *path;
	gboolean ret;

	path = gnome_vfs_get_local_path_from_uri (uri);
	if (!path)
		ret = FALSE;
	else {
		ret = !access (path, R_OK);
		g_free (path);
	}

	return ret;
}

static void
monitor_callback (GnomeVFSMonitorHandle    *handle,
		  const gchar              *monitor_uri,
		  const gchar              *info_uri,
		  GnomeVFSMonitorEventType  event_type,
		  FolderMenuItem         *menuitem)
{
	FolderMenuItemPrivate *priv;
	gchar                   *basename, *mime_type = NULL;

	basename = g_path_get_basename (info_uri);

	if (basename[0] == '.') {
		g_free (basename);

		return;
	}

	g_free (basename);

	if (info_uri)
		mime_type = gnome_vfs_get_mime_type (info_uri);
	else /* a file was deleted */
		mime_type = NULL;

	if (mime_type &&
	    strcmp (mime_type, "x-directory/normal")) {
		g_free (mime_type);

		return;
	}

	priv = FOLDER_MENU_ITEM_GET_PRIVATE (menuitem);
	priv->update_submenu = TRUE;
}

static void
folder_menu_item_select (GtkItem *item)
{
	FolderMenuItem        *menuitem;
	FolderMenuItemPrivate *priv;

	menuitem = FOLDER_MENU_ITEM (item);

	priv = FOLDER_MENU_ITEM_GET_PRIVATE (menuitem);

	if (!priv->monitor_handle)
		gnome_vfs_monitor_add (&priv->monitor_handle,
				       menuitem->uri,
				       GNOME_VFS_MONITOR_DIRECTORY,
				       (GnomeVFSMonitorCallback) monitor_callback,
				       menuitem);

	if (priv->update_submenu &&
	    (!string_contains_uri_delimiter (menuitem->uri) ||
	     g_str_has_prefix (menuitem->uri, "file")) && /* don't read remote URIs */
	     read_ok (menuitem->uri)) /* don't read folder without being permitted */
		update_submenu (menuitem);

	priv->update_submenu = FALSE;

	if (GTK_MENU_ITEM (menuitem)->submenu)
		priv->activation_permitted = FALSE;

	GTK_ITEM_CLASS (parent_class)->select (item);
}

static void
folder_menu_item_deselect (GtkItem *item)
{
	FolderMenuItem *menuitem = FOLDER_MENU_ITEM (item);

	GTK_ITEM_CLASS (parent_class)->deselect (item);
}

static void
folder_menu_item_init (FolderMenuItem *menuitem)
{
	FolderMenuItemPrivate *priv;

	priv = FOLDER_MENU_ITEM_GET_PRIVATE (menuitem);
	priv->activation_permitted = TRUE;
	priv->update_submenu = TRUE;
	priv->monitor_handle = NULL;

	menuitem->uri = NULL;
}

static void
folder_menu_item_class_init (FolderMenuItemClass *class)
{
	GObjectClass     *object_class;
	GtkWidgetClass   *widget_class;
	GtkItemClass     *item_class;
	GtkMenuItemClass *menuitem_class;

	object_class = G_OBJECT_CLASS (class);
	object_class->finalize = folder_menu_item_finalize;

	item_class = GTK_ITEM_CLASS (class);
	item_class->select = folder_menu_item_select;
	item_class->deselect = folder_menu_item_deselect;

	menuitem_class = GTK_MENU_ITEM_CLASS (class);
	menuitem_class->activate = folder_menu_item_activate;

	parent_class = g_type_class_peek_parent (class);

	g_type_class_add_private (class, sizeof (FolderMenuItemPrivate));
}

static void
folder_menu_item_finalize (GObject *object)
{
	FolderMenuItem        *menuitem;
	FolderMenuItemPrivate *priv;

	menuitem = FOLDER_MENU_ITEM (object);

	g_free (menuitem->uri);

	priv = FOLDER_MENU_ITEM_GET_PRIVATE (menuitem);

	if (priv->monitor_handle)
		gnome_vfs_monitor_cancel (priv->monitor_handle);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

GtkWidget *
folder_menu_item_new (const gchar *uri,
		      const gchar *name,
		      const gchar *icon_name)
{
	FolderMenuItem *menuitem;
	GtkWidget        *image;
	GtkWidget        *label;
	GtkIconTheme     *icon_theme;
	GdkPixbuf        *icon_pixbuf;
	gchar            *filename;
	gchar            *basename;
	gchar            *tmpstr;
	int               icon_size;
	GError           *error = NULL;

	g_assert (uri);

	menuitem = g_object_new (TYPE_FOLDER_MENU_ITEM, NULL);

	if (name)
		label = gtk_label_new (name);
	else {
		filename = g_filename_from_uri (uri, NULL, &error);
		if (error) {
			g_warning ("Error during filename conversion: %s",
				   error->message);
			g_error_free (error);
			g_free (filename);

			return NULL;
		}

		basename = g_path_get_basename (filename);

		label = gtk_label_new (basename);

		g_free (filename);
		g_free (basename);
	}

	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);

	gtk_container_add (GTK_CONTAINER (menuitem), label);
	gtk_widget_show (label);

	icon_theme = gtk_icon_theme_get_for_screen (gtk_widget_get_screen (GTK_WIDGET (menuitem)));

	if (icon_name)
		tmpstr = g_strdup (icon_name);
	else
		tmpstr = gnome_icon_lookup_sync (icon_theme, NULL, uri, NULL,
						 GNOME_ICON_LOOKUP_FLAGS_NONE,
						 GNOME_ICON_LOOKUP_RESULT_FLAGS_NONE);

	gtk_icon_size_lookup (GTK_ICON_SIZE_MENU, &icon_size, NULL);
	icon_pixbuf = gtk_icon_theme_load_icon (icon_theme, tmpstr, icon_size, 0, &error);
	if (error) {
		g_warning ("Error when loading icon: %s", error->message);
		g_error_free (error);
	}

	image = gtk_image_new_from_pixbuf (icon_pixbuf);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (menuitem), image);

	gdk_pixbuf_unref (icon_pixbuf);
	g_free (tmpstr);

	menuitem->uri = g_strdup (uri);

	return GTK_WIDGET (menuitem);
}

/* Bookmark Applet - bookmark-menu.h
 *
 * Copyright (C) 2004 Christian Neumair <chris@gnome-de.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef BOOKMARK_MENU_H
#define BOOKMARK_MENU_H

#include <gtk/gtkmenu.h>
#include <gtk/gtkfilechooserdialog.h>
#include <libgnomevfs/gnome-vfs-monitor.h>
#include <libgnomevfs/gnome-vfs-volume-monitor.h>

#define TYPE_BOOKMARK_MENU (bookmark_menu_get_type ())
#define BOOKMARK_MENU(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_BOOKMARK_MENU, BookmarkMenu))
#define BOOKMARK_MENU_CLASS(class)    (G_TYPE_CHECK_CLASS_CAST ((class),  TYPE_BOOKMARK_MENU, BookmarkMenuClass))
#define IS_BOOKMARK_MENU(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_BOOKMARK_MENU))
#define IS_BOOKMARK_MENU_CLASS(class) (G_TYPE_CHECK_CLASS_TYPE ((class),  TYPE_BOOKMARK_MENU))
#define BOOKMARK_MENU_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  TYPE_BOOKMARK_MENU, BookmarkMenuClass))

typedef struct _BookmarkMenu      BookmarkMenu;
typedef struct _BookmarkMenuClass BookmarkMenuClass;

struct _BookmarkMenu {
	GtkMenu parent;

	GnomeVFSMonitorHandle *monitor_handle;
	GnomeVFSVolumeMonitor *volume_monitor;
};

struct _BookmarkMenuClass {
	GtkMenuClass parent_class;

	void (*reread) (BookmarkMenu *menu);
};

GType bookmark_menu_get_type (void);

GtkWidget *bookmark_menu_new (void);

#endif

/* Bookmark Applet - bookmark-applet.c
 *
 * Copyright (C) 2004 Christian Neumair <chris@gnome-de.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
 #include <config.h> /* Internationalization */
#endif

#include "bookmark-menu.h"

#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <panel-applet.h>

static void
edit_bookmarks (BonoboUIComponent *component,
		gpointer           menu,
		const gchar       *cname)
{
	static GtkWidget *filechooser = NULL;

	if (filechooser) {
		gtk_window_present (GTK_WINDOW (filechooser));
		return;
	}

	filechooser = gtk_file_chooser_dialog_new (_("Edit Bookmarks"), NULL,
						   GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
						   GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
						   NULL);
	gtk_file_chooser_set_local_only (GTK_FILE_CHOOSER (filechooser), FALSE);
	gtk_dialog_run (GTK_DIALOG (filechooser));

	gtk_widget_destroy (filechooser);
	filechooser = NULL;
}

static const BonoboUIVerb applet_menu_verbs [] = {
	BONOBO_UI_UNSAFE_VERB ("Edit", edit_bookmarks),
	BONOBO_UI_VERB_END
};

static gboolean
menuitem_button_press_event (GtkWidget      *widget,
			     GdkEventButton *event,
			     gpointer        applet)
{
	gboolean retval;

	if (event->button != 3)
		return FALSE;

	g_signal_stop_emission_by_name (widget,
					"button-press-event");
	g_signal_emit_by_name (GTK_WIDGET (applet),
			       "button-press-event",
			       event, &retval);

	return TRUE;
}

static gboolean
bookmark_applet_fill (PanelApplet *applet,
		      const gchar *iid,
		      gpointer     data)
{
	GtkWidget *menubar;
	GtkWidget *menu;
	GtkWidget *menuitem;
	GtkWidget *image;

	menubar = gtk_menu_bar_new ();
	gtk_container_add (GTK_CONTAINER (applet), menubar);

	menuitem = gtk_image_menu_item_new_with_label (_("Bookmarks"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menubar), menuitem);
	g_signal_connect (menuitem, "button-press-event",
			  G_CALLBACK (menuitem_button_press_event), applet);

	image = gtk_image_new_from_stock (GTK_STOCK_INDEX, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (menuitem), image);

	menu = bookmark_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem), menu);

	panel_applet_setup_menu_from_file (applet,
					   NULL,
					   "GNOME_BookmarkApplet.xml",
					   NULL,
					   applet_menu_verbs,
					   menu);

	gtk_widget_show_all (GTK_WIDGET (applet));

	return TRUE;
}

PANEL_APPLET_BONOBO_FACTORY ("OAFIID:GNOME_BookmarkApplet_Factory",
			     PANEL_TYPE_APPLET,
			     "GNOME_BookmarkApplet",
			     0,
			     bookmark_applet_fill,
			     NULL)

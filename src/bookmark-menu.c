/* Bookmark Applet - bookmark-menu.c
 *
 * Copyright (C) 2004 Christian Neumair <chris@gnome-de.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
 #include <config.h> /* Internationalization */
#endif

#include "bookmark-menu.h"
#include <lib/folder-menu-item.h>

#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <libgnomevfs/gnome-vfs.h>

enum {
	REREAD,
	LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

static gchar *bookmark_path = NULL;

/* Helpers */
static void bookmark_file_monitor (GnomeVFSMonitorHandle    *handle,
				   const gchar              *monitor_uri,
				   const gchar              *info_uri,
				   GnomeVFSMonitorEventType  event_type,
				   gpointer                  menu);


/* Static Methods */
static void bookmark_menu_show       (GtkWidget         *widget);
static void bookmark_menu_init       (BookmarkMenu      *menu);
static void bookmark_menu_class_init (BookmarkMenuClass *class);
static void bookmark_menu_finalize   (GObject           *object);

/* Callbacks */
static void bookmark_menu_reread (BookmarkMenu *menu);

G_DEFINE_TYPE (BookmarkMenu, bookmark_menu, GTK_TYPE_MENU);

static GtkMenuClass *parent_class = NULL;

static void
bookmark_menu_reread (BookmarkMenu *menu)
{
	GtkWidget *menuitem;
	gchar     *tmpstr, *tmpstr2, *tmpstr3;
	GError    *error = NULL;

	/* VFS stuff */
	GnomeVFSVolume *vfs_volume;
	GList          *list, *l;
	GSList         *vfs_volumes = NULL, *ls;

	/* ~/.gtk-bookmarks */
	gchar **bookmarks = NULL, **bookmark_info = NULL;
	guint   i = 0;

	gtk_container_foreach (GTK_CONTAINER (menu),
			       (GtkCallback) gtk_widget_destroy,
			       NULL);

	/* Default bookmarks */
	tmpstr = g_build_path (G_DIR_SEPARATOR_S, "file://", g_get_home_dir (), NULL);
	menuitem = folder_menu_item_new (tmpstr, _("Home"), "gnome-fs-home");
	gtk_widget_show (menuitem);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	g_free (tmpstr);

	tmpstr = g_build_path (G_DIR_SEPARATOR_S, "file://", g_get_home_dir (), "Desktop", NULL);
	menuitem = folder_menu_item_new (tmpstr, _("Desktop"), "gnome-fs-desktop");
	gtk_widget_show (menuitem);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	g_free (tmpstr);

	/* only display already mounted volumes as in gnome-panel places menu
	 * avoids accidentally mounting drives by just moving the mouse
	 */

	list = gnome_vfs_volume_monitor_get_mounted_volumes (menu->volume_monitor);

	for (l = list; l; l = l->next) {
		vfs_volume = GNOME_VFS_VOLUME (l->data);

		if (gnome_vfs_volume_is_user_visible (vfs_volume) &&
			gnome_vfs_volume_is_mounted (vfs_volume)) {
			vfs_volumes = g_slist_prepend (vfs_volumes, vfs_volume);
		}
		else
			gnome_vfs_volume_unref (vfs_volume);
	}

	g_list_free (list);

	vfs_volumes = g_slist_reverse (vfs_volumes);

	vfs_volume = gnome_vfs_volume_monitor_get_volume_for_path (menu->volume_monitor, "/");
	vfs_volumes = g_slist_prepend (vfs_volumes, vfs_volume);

	for (ls = vfs_volumes; ls; ls = ls->next) {
		vfs_volume = GNOME_VFS_VOLUME (ls->data);

		tmpstr = gnome_vfs_volume_get_activation_uri (vfs_volume);
		tmpstr2 = gnome_vfs_volume_get_display_name (vfs_volume);
		tmpstr3 = gnome_vfs_volume_get_icon (vfs_volume);

		menuitem = folder_menu_item_new (tmpstr,
						   !strcmp (tmpstr, "file:///") ?
						     _("Filesystem") : tmpstr2,
						   tmpstr3);
		gtk_widget_show (menuitem);
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);

		g_free (tmpstr);
		g_free (tmpstr2);
		g_free (tmpstr3);
	}

	if (!g_file_test (bookmark_path,
			  G_FILE_TEST_EXISTS))
		return;

	/* read ~/.gtk-bookmarks */
	if (!g_file_get_contents (bookmark_path,
				  &tmpstr,
				  NULL,
				  &error)) {
		g_warning ("Error during bookmark file parsing: %s",
			   error->message);
		g_error_free (error);

		if (tmpstr)
			g_free (tmpstr);

		return;
	}

	bookmarks = g_strsplit (tmpstr, "\n", 0);

	if (bookmarks[0] && bookmarks[0][0]) {
		menuitem = gtk_separator_menu_item_new ();
		gtk_widget_show (menuitem);
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	}

	for (i = 0; bookmarks[i]; i++) {
		if (!(bookmarks[i][0]))
			continue;
		
		bookmark_info = g_strsplit (bookmarks[i], " ", 2);

		menuitem = folder_menu_item_new (bookmark_info[0], bookmark_info[1], NULL);
		gtk_widget_show (menuitem);
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
		
		g_strfreev (bookmark_info);
	}

	g_free (tmpstr);
	g_strfreev (bookmarks);

	return;
}

static void
bookmark_file_monitor (GnomeVFSMonitorHandle    *handle,
		       const gchar              *monitor_uri,
		       const gchar              *info_uri,
		       GnomeVFSMonitorEventType  event_type,
		       gpointer                  menu)
{
	g_signal_emit_by_name (menu, "reread", NULL);
}


static void
bookmark_menu_show (GtkWidget *widget)
{

	GTK_WIDGET_CLASS (parent_class)->show (widget);
}

static void
bookmark_menu_init (BookmarkMenu *menu)
{
	menu->monitor_handle = NULL;
	menu->volume_monitor = NULL;

	if (!bookmark_path)
		bookmark_path = g_build_filename (g_get_home_dir (),
						  ".gtk-bookmarks",
						  NULL);

	gnome_vfs_init ();

	gnome_vfs_monitor_add (&(menu->monitor_handle),
			       bookmark_path,
			       GNOME_VFS_MONITOR_FILE,
			       bookmark_file_monitor,
			       menu);

	menu->volume_monitor = gnome_vfs_volume_monitor_ref (gnome_vfs_get_volume_monitor ());

	g_signal_emit_by_name (menu, "reread", NULL);
}

static void
bookmark_menu_class_init (BookmarkMenuClass *class)
{
	GObjectClass      *object_class;
	GtkWidgetClass    *widget_class;

	class->reread = bookmark_menu_reread;

	parent_class = g_type_class_peek_parent (class);

	object_class = G_OBJECT_CLASS (class);
	object_class->finalize = bookmark_menu_finalize;

	widget_class = GTK_WIDGET_CLASS (class);
	widget_class->show = bookmark_menu_show;

	signals[REREAD] =
		g_signal_new ("reread",
			      G_TYPE_FROM_CLASS (class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (BookmarkMenuClass, reread),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__OBJECT,
			      G_TYPE_NONE, 1,
			      TYPE_BOOKMARK_MENU);
}

static void
bookmark_menu_finalize (GObject *object)
{
	BookmarkMenu *menu = BOOKMARK_MENU (object);

	if (menu->monitor_handle)
		gnome_vfs_monitor_cancel (menu->monitor_handle);

	if (menu->volume_monitor)
		gnome_vfs_volume_monitor_unref (menu->volume_monitor);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

GtkWidget *
bookmark_menu_new (void)
{
	return g_object_new (TYPE_BOOKMARK_MENU, NULL);
}
